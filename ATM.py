def handle(s: str) -> str:
    if len(s) == 1:
        return s
    res = ""

    d = []
    i = 1
    count = 1
    while i < len(s):
        if s[i] != s[i - 1]:
            d.append((s[i - 1], count))
            count = 1
        else:
            count += 1
        i += 1
    d.append((s[-1], count))

    if len(d) == 1:
        return s

    i = 1
    while i < len(d):
        pre_char, pre_num = d[i - 1]
        char, num = d[i]
        if pre_char > char:
            res += pre_char * pre_num
        else:
            res += pre_char * (pre_num * 2)
        i += 1
    last_char, last_num = d[-1]
    res += last_char * last_num
    return res


def process():
    t = int(input())
    data = []
    for i in range(int(t)):
        data.append(str(input()))

    for index, val in enumerate(data):
        res = handle(val)
        print("Case #{}: {}".format(index + 1, res))


process()
